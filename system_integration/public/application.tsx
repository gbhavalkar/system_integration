import React from 'react';
import ReactDOM, { render } from 'react-dom';
import { AppMountParameters } from '../../../src/core/public';
import { AppPluginStartDependencies } from './types';
import { SystemIntegrationApp } from './components/app';
import { AppDependencies } from './application_dependencies';
import { KibanaContextProvider } from '../../../src/plugins/kibana_react/public';

export const renderApp = (
  { element, appBasePath }: AppMountParameters,
  appDependencies: AppDependencies,
  { navigation }: AppPluginStartDependencies
) => {
  render(
    <KibanaContextProvider services={appDependencies}>
      <SystemIntegrationApp
        basename={appBasePath}
        notifications={appDependencies.notifications}
        application={appDependencies.application}
        http={appDependencies.http}
        navigation={navigation}
        history={appDependencies.history}
      />
    </KibanaContextProvider>,
    element
  );

  return () => ReactDOM.unmountComponentAtNode(element);
};
