import { createContext } from "react";
import { ConsoleUserProfile } from "../../common/types/user_profile";

const defaultValue = {
  customerId: '',
  customerName: '',
  azUserId: '',
  emailAddress: '',
  emulatedByAppzenSuperAdmin: false,
  emulatedByPartnerAdmin: false,
  emulatedByEmulator: false,
  firstName: '',
  lastName: '',
  roles: [],
};

export const userContext = createContext<ConsoleUserProfile | undefined>(defaultValue);
