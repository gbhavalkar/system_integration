import { i18n } from '@kbn/i18n';
import { AppMountParameters, CoreSetup, CoreStart, Plugin } from '../../../src/core/public';
import {
  SystemIntegrationPluginSetup,
  SystemIntegrationPluginStart,
  AppPluginStartDependencies,
} from './types';
import { PLUGIN_NAME } from '../common';
import { textService } from './services/text';
import { breadcrumbService, docTitleService } from './services/navigation';
import { AppDependencies } from './application_dependencies';

export class SystemIntegrationPlugin
  implements Plugin<SystemIntegrationPluginSetup, SystemIntegrationPluginStart> {
  public setup(coreSetup: CoreSetup): SystemIntegrationPluginSetup {
    // Register an application into the side navigation menu
    coreSetup.application.register({
      id: 'systemIntegration',
      title: PLUGIN_NAME,
      async mount(params: AppMountParameters) {
        // Load application bundle
        const { renderApp } = await import('./application');

        const { history } = params;
        const { http, notifications, getStartServices } = coreSetup;
        const startServices = await getStartServices();
        const [core, depsStart] = startServices;
        const { chrome, docLinks, i18n, overlays, savedObjects, uiSettings, application } = core;
        const { docTitle, setBreadcrumbs } = chrome;

        //Initialize services
        textService.init();
        docTitleService.init(docTitle.change);
        breadcrumbService.setup(setBreadcrumbs);

        // AppCore/AppPlugins to be passed on as React context
        const appDependencies: AppDependencies = {
          chrome,
          docLinks,
          http,
          i18n,
          notifications,
          overlays,
          savedObjects,
          uiSettings,
          history,
          application,
        };

        // Render the application
        return renderApp(params, appDependencies, depsStart as AppPluginStartDependencies);
      },
    });

    // Return methods that should be available to other plugins
    return {
      getGreeting() {
        return i18n.translate('systemIntegration.greetingText', {
          defaultMessage: 'Hello from {name}!',
          values: {
            name: PLUGIN_NAME,
          },
        });
      },
    };
  }

  public start(core: CoreStart): SystemIntegrationPluginStart {
    return {};
  }

  public stop() {}
}
