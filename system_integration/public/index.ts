import './index.scss';

import { SystemIntegrationPlugin } from './plugin';

// This exports static code and TypeScript types,
// as well as, Kibana Platform `plugin()` initializer.
export function plugin() {
  return new SystemIntegrationPlugin();
}
export { SystemIntegrationPluginSetup, SystemIntegrationPluginStart } from './types';
