import { textService } from '../text';

class DocTitleService {
  private changeDocTitle: any = () => {};

  public init(changeDocTitle: any): void {
    this.changeDocTitle = changeDocTitle;
  }

  public setTitle(page?: string): void {
    if (!page || page === 'vat') {
      this.changeDocTitle(`${textService.breadcrumbs.vatWorkbench}`);
    } else if (textService.breadcrumbs[page]) {
      this.changeDocTitle(`${textService.breadcrumbs[page]} - ${textService.breadcrumbs.vatWorkbench}`);
    }
  }
}

export const docTitleService = new DocTitleService();
