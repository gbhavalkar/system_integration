import { ManagementAppMountParams } from '../../../../../../../src/plugins/management/public';

type SetBreadcrumbs = ManagementAppMountParams['setBreadcrumbs'];


export enum BREADCRUMB_SECTION {
}

interface BreadcrumbItem {
  text: string;
  href?: string;
  onClick?: (event: MouseEvent) => void;
}



class BreadcrumbService {
  private setBreadcrumbsHandler?: SetBreadcrumbs;

  public setup(setBreadcrumbsHandler: SetBreadcrumbs): void {
    this.setBreadcrumbsHandler = setBreadcrumbsHandler;
  }

  public setBreadcrumbs(newBreadcrumbs: BreadcrumbItem[]): void {
    if (!this.setBreadcrumbsHandler) {
      throw new Error(`BreadcrumbService#setup() must be called first!`);
    }

    this.setBreadcrumbsHandler(newBreadcrumbs);
  }
}

export const breadcrumbService = new BreadcrumbService();
