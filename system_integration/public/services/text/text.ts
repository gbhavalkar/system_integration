import { i18n } from '@kbn/i18n';

class TextService {
  public breadcrumbs: { [key: string]: string } = {};

  public init(): void {
    this.breadcrumbs = {
      setupAndAdmin: i18n.translate('navigation.breadcrumbs.title.systemIntegration', {
        defaultMessage: 'System Integration',
      }), 
    };
  }
}

export const textService = new TextService();
