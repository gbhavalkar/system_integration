import { CoreSetup, CoreStart } from 'src/core/public';
import { ScopedHistory } from 'kibana/public';
import { useKibana } from '../../../src/plugins/kibana_react/public';
import { Storage } from '../../../src/plugins/kibana_utils/public';
import { ApplicationStart } from 'src/core/public/application';

export interface AppDependencies {
  chrome: CoreStart['chrome'];
  docLinks: CoreStart['docLinks'];
  http: CoreSetup['http'];
  i18n: CoreStart['i18n'];
  notifications: CoreSetup['notifications'];
  uiSettings: CoreStart['uiSettings'];
  savedObjects: CoreStart['savedObjects'];
  storage?: Storage;
  overlays: CoreStart['overlays'];
  history: ScopedHistory;
  application: ApplicationStart;
}

export const useAppDependencies = () => {
  return useKibana().services as AppDependencies;
};

export const useToastNotifications = () => {
  const {
    notifications: { toasts: toastNotifications },
  } = useAppDependencies();
  return toastNotifications;
};
