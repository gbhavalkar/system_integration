import { useMemo } from "react";
import { useAppDependencies } from "../application_dependencies";
import { INTEGRATIONS_API_BASE_PATH } from "../../common/constants/common";
import { GetIntegrationObjectsSchema } from "../../common/api_schemas/integration_object";

export const useIntegrationObjectAPI = () => {
  const { http } = useAppDependencies();
  return useMemo(() => ({
    getIntegrationObjects(): Promise<GetIntegrationObjectsSchema> {
      return http.get(`${INTEGRATIONS_API_BASE_PATH}integration-objects`, {
        query: {
          pageSize: 10,
          pageNumber: 1
        }
      });
    },
  }), [http]);
}