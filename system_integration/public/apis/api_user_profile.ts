import { useMemo } from "react";
import { useAppDependencies } from "../application_dependencies";
import { USER_API_BASE_PATH } from "../../common/constants/common";

export const useUserProfileAPI = () => {
  const { http } = useAppDependencies();
  return useMemo(() => ({
    getUserProfile(): Promise<any> {
      return http.get(`${USER_API_BASE_PATH}user`);
    },
  }), [http]);
}