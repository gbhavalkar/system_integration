import React, { useEffect, useState } from 'react';
import { I18nProvider } from '@kbn/i18n/react';
import { Router, Route, Switch } from 'react-router-dom';
import { CoreStart, ScopedHistory, ApplicationStart } from '../../../../src/core/public';
import { NavigationPublicPluginStart } from '../../../../src/plugins/navigation/public';
import { ConsoleUserProfile } from '../../common/types/user_profile';
import { useGetUserProfile } from '../hooks/use_get_user_profile';
import { userContext } from '../context/user_context';
import IntegrationsLandingPage from '../pages/integrations_landing/integrations_landing_page';

interface SystemIntegrationAppDeps {
  basename: string;
  notifications: CoreStart['notifications'];
  application: ApplicationStart;
  http: CoreStart['http'];
  navigation: NavigationPublicPluginStart;
  history: ScopedHistory;
}

export const SystemIntegrationApp = ({
  history,
  notifications,
  http,
  navigation,
}: SystemIntegrationAppDeps) => {
  const [userProfile, setUserProfile] = useState<ConsoleUserProfile | undefined>();
  const [userProfileError, setUserProfileError] = useState<any>();

  const getUserProfile = useGetUserProfile(setUserProfile, setUserProfileError);

  useEffect(() => {
    getUserProfile();
  }, []);

  return (
    <Router history={history}>
      <userContext.Provider value={!userProfileError ? userProfile : undefined}>
        <I18nProvider>
          <Switch>
            <Route path="/">
              <IntegrationsLandingPage />
            </Route>
          </Switch>
        </I18nProvider>
      </userContext.Provider>
    </Router>
  );
};
