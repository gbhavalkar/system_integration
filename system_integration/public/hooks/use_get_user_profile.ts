import React from 'react';
import { HttpFetchError } from 'kibana/public';
import { useUserProfileAPI } from '../apis/api_user_profile';
import { ConsoleUserProfile } from '../../common/types/user_profile';
import { isValidUserProfile } from '../../common/types/type_guards';

export const useGetUserProfile = (
  setUserProfile: React.Dispatch<React.SetStateAction<ConsoleUserProfile | undefined>>,
  setErrorMessage: React.Dispatch<React.SetStateAction<HttpFetchError | undefined>>
) => {
  const userAPI = useUserProfileAPI();

  const getUserProfile = async () => {
    try {
      const userProfileDataResp = await userAPI.getUserProfile();
      const userProfile = isValidUserProfile(userProfileDataResp)
        ? (userProfileDataResp as ConsoleUserProfile)
        : undefined;
      setUserProfile(userProfile);
      setErrorMessage(undefined);
    } catch (e) {
      setUserProfile(undefined);
      setErrorMessage(e as HttpFetchError);
    }
  };

  return getUserProfile;
}