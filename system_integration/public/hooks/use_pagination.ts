import { isFunction } from 'lodash';
import { useEffect, useState } from 'react';

export const usePagination = (
  initialPageSize: number = 10,
  pageSizeOptions: Array<number> | undefined = [10, 25, 50],
  handleChange: (pageNumber: number, pageSize: number) => void,
) => {
  const [activePage, setActivePage] = useState<number>(0);
  const [pageSize, setPageSize] = useState<number>(initialPageSize);
  const [totalItemCount, setTotalItemCount] = useState<number>(0);

  const onChange = ({ page = { index: 0, size: pageSize }, sort = {} }):void => {
    const { index: pageIndex, size: pageSize } = page;

    setActivePage(pageIndex);
    setPageSize(pageSize);
  }

  useEffect(() => {
    if (isFunction(handleChange)) {
      handleChange(activePage, pageSize);
    }
    return () => {
    }
  }, [activePage, pageSize]);

  return {
    pageIndex: activePage,
    pageSize: pageSize,
    totalItemCount,
    setTotalItemCount,
    pageSizeOptions,
    onChange
  }
}