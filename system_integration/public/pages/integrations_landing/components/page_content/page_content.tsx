import React, { useState } from 'react';
import { EuiTab, EuiTabs } from '@elastic/eui';
import styled from 'styled-components';
import IntegrationObjectWorkbench from '../integration_object/integration_object_workbench/integration_object_workbench';
import { find } from 'lodash';

type Props = {};

const StyledTab = styled(EuiTab)<{ isSelected: boolean }>`
  font-size: 14px;
  border: 1px solid #ececec;
  background-color: ${(props) => (props.isSelected ? 'white' : 'inherit')};
`;
const TabContentWrapper = styled.div`
  height: 100%;
`;

const IntegrationsPageContent = (props: Props) => {
  const [selectedTabId, setSelectedTabId] = useState<number>(1);
  const tabs = [
    {
      id: 1,
      name: 'Integration Object',
      content: <IntegrationObjectWorkbench />,
    },
    {
      id: 2,
      name: 'Autonomous AP Scheduling',
      content: <span></span>,
    },
  ];

  const onSelectedTabChanged = (id: number) => setSelectedTabId(id);

  const renderTabs = () =>
    tabs.map((tab, index) => (
      <StyledTab
        onClick={() => onSelectedTabChanged(tab.id)}
        isSelected={tab.id === selectedTabId}
        key={index}
      >
        {tab.name}
      </StyledTab>
    ));

  const TabContent = find(tabs, ['id', selectedTabId])?.content;
  return (
    <>
      <EuiTabs>{renderTabs()}</EuiTabs>
      <TabContentWrapper>{TabContent}</TabContentWrapper>
    </>
  );
};

export default IntegrationsPageContent;
