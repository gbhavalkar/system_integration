import { EuiBasicTable, EuiButton, EuiFlexGroup, EuiFlexItem, EuiIcon } from '@elastic/eui';
import { IntegrationObject } from '../../../../../../common/types/integration_object';
import { usePagination } from '../../../../../hooks/use_pagination';
import React, { useState } from 'react';
import styled from 'styled-components';
import { useIntegrationObjectColumns } from './use_integration_object_columns';
import { useIntegrationObjectAPI } from '../../../../../apis/api_integration_object';
import { GetIntegrationObjectsSchema } from '../../../../../../common/api_schemas/integration_object';
import { FormattedMessage } from 'react-intl';

type Props = {};

const WorkbenchWrapper = styled.div`
  height: 100%;
  background-color: white;
  border: 1px solid #ececec;
  padding: 2%;
`;
const AddNewIcon = styled(EuiIcon)`
  margin-right: 5px;
`;
const AddNewButton = styled(EuiButton)`
  width: fit-content;
  box-shadow: none !important;
  border: none;
  font-size: 13px;
`;

const IntegrationObjectWorkbench = (props: Props) => {
  const [isFetching, setIsFetching] = useState<boolean>(false);
  const [integrationObjects, setIntegrationObjects] = useState<IntegrationObject[]>([]);
  const workbenchColumns = useIntegrationObjectColumns();
  const {
    pageIndex,
    pageSize,
    totalItemCount,
    setTotalItemCount,
    pageSizeOptions,
    onChange,
  } = usePagination(10, undefined, handleChange);
  const api = useIntegrationObjectAPI();

  async function fetchIntegrationObjects({
    pageIndex,
    pageSize,
  }: {
    pageIndex: number;
    pageSize: number;
  }) {
    try {
      setIsFetching(true);
      const integrationsObjectResponse: GetIntegrationObjectsSchema = await api.getIntegrationObjects();
      setIntegrationObjects(integrationsObjectResponse.results);
      setTotalItemCount(integrationsObjectResponse.count);
      setIsFetching(false);
    } catch (error) {
      setIsFetching(false);
      console.error(error);
    }
  }

  function handleChange() {
    fetchIntegrationObjects({ pageIndex, pageSize });
  }

  return (
    <WorkbenchWrapper>
      <EuiFlexGroup direction="column">
        <EuiFlexItem>
          <AddNewButton>
            <AddNewIcon size="s" color="#1C24D3" type="plusInCircle" />
            <FormattedMessage
              id={`mastermind.systemIntegration.app.integrationsLanding.integrationObject.addNewIntegrationObject`}
              defaultMessage={'Add New Integration Object'}
            />
          </AddNewButton>
        </EuiFlexItem>
        <EuiFlexItem>
          <EuiBasicTable
            columns={workbenchColumns}
            items={integrationObjects}
            rowProps={() => ({ className: 'integration-object--row' })}
            cellProps={() => ({ className: 'integration-object--cell' })}
            noItemsMessage={
              isFetching ? 'Fetching Integration Objects...' : 'No Integration Objects configured.'
            }
            loading={isFetching}
            isExpandable={true}
            onChange={onChange}
            isSelectable={false}
            pagination={{ pageIndex, pageSize, totalItemCount, pageSizeOptions }}
          />
        </EuiFlexItem>
      </EuiFlexGroup>
    </WorkbenchWrapper>
  );
};

export default IntegrationObjectWorkbench;
