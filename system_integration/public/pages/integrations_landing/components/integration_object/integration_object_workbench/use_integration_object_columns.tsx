import React from 'react';
import { FormattedMessage } from 'react-intl';
import { EuiButtonIcon, EuiTableDataType } from '@elastic/eui';
import styled from 'styled-components';
import { EditIcon } from '../../../../../components/icons/EditIcon';
import { IntegrationObjectWorkbenchFields } from '../../../../../../common/constants/integration_object';
import { IntegrationObject } from '../../../../../../common/types/integration_object';

const StringDataType: EuiTableDataType = 'string';

const ColumnTitle = styled.span`
  font-size: 12px;
`;

const renderColumnTitle = (columnKey: string, defaultValue: string) => (
  <ColumnTitle>
    <FormattedMessage
      id={`mastermind.systemIntegration.app.integrationsLanding.integrationObject.workbenchColumns.${columnKey}`}
      defaultMessage={defaultValue}
    />
  </ColumnTitle>
);

export const useIntegrationObjectColumns = () => {
  return [
    {
      name: renderColumnTitle('integrationName', 'Name'),
      field: IntegrationObjectWorkbenchFields.INTEGRATION_OBJECT_NAME,
      dataType: StringDataType,
      truncateText: true,
      width: '50%',
    },
    {
      name: renderColumnTitle('integrationType', 'Type'),
      field: IntegrationObjectWorkbenchFields.INTEGRATION_OBJECT_TYPE,
      dataType: StringDataType,
      truncateText: true,
      width: '8%',
    },
    {
      name: renderColumnTitle('actions', 'Actions'),
      width: '15%',
      actions: [
        {
          name: <span></span>,
          description: 'Edit',
          onClick: (integrationObject: IntegrationObject) => {
            alert(`${integrationObject.integration_name} has been selected for Edit !!!!`);
          },
          'data-test-subj': 'action-edit',
          render(integrationObject: IntegrationObject) {
            return (
              <EuiButtonIcon
                color="primary"
                iconType={EditIcon}
                size="s"
                aria-label="Edit"
                onClick={() => {
                  alert(`${integrationObject.integration_name} has been selected for Edit !!!!`);
                }}
              />
            );
          },
        },
        {
          name: <span></span>,
          description: 'Delete',
          onClick: (integrationObject: IntegrationObject) => {
            alert(`${integrationObject.integration_name} has been selected for Delete !!!!`);
          },
          'data-test-subj': 'action-edit',
          render(integrationObject: IntegrationObject) {
            return (
              <EuiButtonIcon
                color='danger'
                iconType='trash'
                size="s"
                aria-label="Delete"
                onClick={() => {
                  alert(`${integrationObject.integration_name} has been selected for Delete !!!!`);
                }}
              />
            );
          },
        },
      ],
    },
  ];
};
