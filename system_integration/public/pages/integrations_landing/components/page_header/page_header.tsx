import React from 'react';
import { FormattedMessage } from 'react-intl';
import { EuiFlexGroup, EuiFlexItem, EuiTitle } from '@elastic/eui';
import styled from 'styled-components';

type Props = {
  customerName: string;
};

const PageTitleWrapper = styled.span`
  color: #101011;
  font-weight: 600;
  font-size: 20px;
  letter-spacing: 0.1px;
  line-height: 28px;
`;
const CustomerNameWrapper = styled.span`
  color: #101011;
  font-size: 14px;
  font-weight: 400;
`;
const PageHeaderWrapper = styled(EuiFlexGroup)`
  width: max-content;
`;
const PageHeaderSectionLeft = styled(EuiFlexItem)`
  min-width: max-content;
`;
const PageHeaderSectionRight = styled(EuiFlexItem)`
  min-width: max-content;
  padding-left: 1%;
`;

const IntegrationsPageHeader = ({ customerName }: Props) => {
  return (
    <PageHeaderWrapper direction="row" justifyContent="center" alignItems="center">
      <PageHeaderSectionLeft>
        <EuiTitle>
          <PageTitleWrapper>
            <FormattedMessage
              id="mastermind.systemIntegration.app.integrationsLanding.headerTitle"
              defaultMessage="System Integrations"
            />
          </PageTitleWrapper>
        </EuiTitle>
      </PageHeaderSectionLeft>
      <PageHeaderSectionRight>
        <CustomerNameWrapper>
          <FormattedMessage
            id="mastermind.systemIntegration.app.integrationsLanding.customerTitle"
            defaultMessage="Customer"
          />
          : {customerName}
        </CustomerNameWrapper>
      </PageHeaderSectionRight>
    </PageHeaderWrapper>
  );
};

export default IntegrationsPageHeader;
