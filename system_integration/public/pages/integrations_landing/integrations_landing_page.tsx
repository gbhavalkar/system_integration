import React, { useContext } from 'react';
import { EuiFlexGroup, EuiPage, EuiPageBody, EuiPanel, EuiFlexItem } from '@elastic/eui';
import styled from 'styled-components';
import IntegrationsPageHeader from './components/page_header/page_header';
import { userContext } from '../../context/user_context';
import IntegrationsPageContent from './components/page_content/page_content';

const Page = styled(EuiPage)`
  position: relative;
  overflow: hidden;
`;
const MainContainer = styled.div`
  display: flex;
  flex: 1 1 0%;
  overflow: auto;
`;
const PanelContainer = styled(EuiPanel)`
  background-color: #fafbfd !important;
  padding: 20px;
  height: calc(100vh - 125px);
  width: 85%;
  border: none;
`;
const PageWrapper = styled(EuiFlexGroup)`
  margin-left: 1%;
  margin-right: 1%;
  height: 100%;
`;
const PageHeaderWrapper = styled(EuiFlexItem)`
  max-height: 5%;
`;
const PageContentWrapper = styled(EuiFlexItem)``;

interface Props {}

const IntegrationsLandingPage = (props: Props) => {
  const userProfile = useContext(userContext);
  return (
    <Page>
      <EuiPageBody>
        <MainContainer>
          <PanelContainer>
            <PageWrapper direction="column">
              <PageHeaderWrapper>
                <IntegrationsPageHeader
                  customerName={userProfile ? userProfile.customerName : ''}
                />
              </PageHeaderWrapper>
              <PageContentWrapper>
                <IntegrationsPageContent />
              </PageContentWrapper>
            </PageWrapper>
          </PanelContainer>
        </MainContainer>
      </EuiPageBody>
    </Page>
  );
};

export default IntegrationsLandingPage;
