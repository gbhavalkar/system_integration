import { NavigationPublicPluginStart } from '../../../src/plugins/navigation/public';

export interface SystemIntegrationPluginSetup {
  getGreeting: () => string;
}
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface SystemIntegrationPluginStart {}

export interface AppPluginStartDependencies {
  navigation: NavigationPublicPluginStart;
}
