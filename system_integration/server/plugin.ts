import {
  PluginInitializerContext,
  CoreSetup,
  CoreStart,
  Plugin,
  Logger,
  IRouter
} from '../../../src/core/server';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { SystemIntegrationPluginSetup, SystemIntegrationPluginStart } from './types';
import { ApiRoutes } from './routes';
import { ConfigSchema } from './config';

export class SystemIntegrationPlugin implements Plugin<SystemIntegrationPluginSetup, SystemIntegrationPluginStart> {
  private readonly logger: Logger;
  private readonly apiRoutes: ApiRoutes;
  private readonly configObservable: Observable<ConfigSchema>;
  private config!: ConfigSchema;
  private router!: IRouter;

  constructor(initializerContext: PluginInitializerContext) {
    this.configObservable = initializerContext.config.create();
    this.logger = initializerContext.logger.get();
    this.apiRoutes = new ApiRoutes();
  }

  public async setup(core: CoreSetup) {
    this.logger.debug('ApprovalAppPlugin: Setup');
    this.router = core.http.createRouter();
    this.config = await this.configObservable.pipe(first()).toPromise();
    this.logger.info("ApprovalAppPlugin Config: " + JSON.stringify(this.config));


    return {};
  }

  public start(core: CoreStart) {
    this.logger.debug('systemIntegration: Started');
    this.apiRoutes.setup({
      router: this.router,
      config: this.config,
      auth: core.http.auth,
      logger: this.logger,
    });

    return {};
  }

  public stop() {}
}
