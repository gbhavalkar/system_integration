import { IntegrationTypes, TransportationMethods } from "../../common/constants/integration_object";
import { IntegrationObject } from "../../common/types/integration_object";
import { ESSearchResult } from "../types";

export const mockIntegrationObjectResponse: ESSearchResult<IntegrationObject> = {
  body: {
    hits: {
      total: {
        value: 15,
      },
      hits: [
        {
          _id: 'document_id_1',
          _source: {
            customer_id: '3283',
            integration_id: 'integration_id_1',
            integration_name: 'CSV 1',
            integration_type: IntegrationTypes.CSV,
            transportation_method: TransportationMethods.SFTP
          }
        },
        {
          _id: 'document_id_2',
          _source: {
            customer_id: '3283',
            integration_id: 'integration_id_2',
            integration_name: 'CSV 2',
            integration_type: IntegrationTypes.CSV,
            transportation_method: TransportationMethods.SFTP
          }
        },
        {
          _id: 'document_id_3',
          _source: {
            customer_id: '3283',
            integration_id: 'integration_id_3',
            integration_name: 'Coupa 1',
            integration_type: IntegrationTypes.COUPA
          }
        },
        {
          _id: 'document_id_4',
          _source: {
            customer_id: '3283',
            integration_id: 'integration_id_4',
            integration_name: 'Coupa 2',
            integration_type: IntegrationTypes.COUPA
          }
        },
        {
          _id: 'document_id_5',
          _source: {
            customer_id: '3283',
            integration_id: 'integration_id_5',
            integration_name: 'CSV 3',
            integration_type: IntegrationTypes.CSV,
            transportation_method: TransportationMethods.SFTP
          }
        },
        {
          _id: 'document_id_6',
          _source: {
            customer_id: '3283',
            integration_id: 'integration_id_6',
            integration_name: 'CSV 4',
            integration_type: IntegrationTypes.CSV,
            transportation_method: TransportationMethods.SFTP
          }
        },
        {
          _id: 'document_id_7',
          _source: {
            customer_id: '3283',
            integration_id: 'integration_id_7',
            integration_name: 'Coupa 3',
            integration_type: IntegrationTypes.COUPA
          }
        },
        {
          _id: 'document_id_8',
          _source: {
            customer_id: '3283',
            integration_id: 'integration_id_8',
            integration_name: 'Coupa 4',
            integration_type: IntegrationTypes.COUPA
          }
        },
        {
          _id: 'document_id_9',
          _source: {
            customer_id: '3283',
            integration_id: 'integration_id_9',
            integration_name: 'CSV 5',
            integration_type: IntegrationTypes.CSV,
            transportation_method: TransportationMethods.SFTP
          }
        },
        {
          _id: 'document_id_10',
          _source: {
            customer_id: '3283',
            integration_id: 'integration_id_10',
            integration_name: 'CSV 6',
            integration_type: IntegrationTypes.CSV,
            transportation_method: TransportationMethods.SFTP
          }
        },
      ]
    }
  }
}