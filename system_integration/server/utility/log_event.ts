import { Logger } from 'src/core/server';
import { find, flow, get } from 'lodash/fp';
import moment from 'moment';
import isDefined from '../../common/utility/isDefined';
import { AuthUserDetails } from '../types';
import { ISO_DATE_FORMAT } from '../constants/date';
import { LOG_LEVEL_TYPE, RESPONSE_STATUS } from '../constants/log';

interface LogDetails {
  level: LOG_LEVEL_TYPE;
  status_code?: RESPONSE_STATUS;
  event: string;
  msg: string;
  user_details: AuthUserDetails;
  api_endpoint?: string;
  api_method?: string;
  start_time?: string;
}

export const logEvent = (log: LogDetails, otherLogDetails: any = {}, logger?: Logger) => {
  const entitySettings = flow(get('user_details.entitySettings'))(log) || [];
  const AAAUserEntities = flow(find(['app_name', 'AAA']), get('entities'))(entitySettings) || [];
  const endTime = moment().utc().format(ISO_DATE_FORMAT);

  const newLog = {
    level: log.level || LOG_LEVEL_TYPE.INFO,
    status_code: log.status_code,
    event: log.event,
    msg: log.msg,
    customer_id: log.user_details?.customerId,
    customer_name: log.user_details?.customerName,
    az_user_id: log.user_details?.azUserId,
    user_roles: log.user_details?.roles,
    user_entities: AAAUserEntities,
    is_free_trial_user:
      log.user_details?.createdBy?.toLowerCase() === 'free_trial_account' ||
      log.user_details?.customerName?.toLowerCase().includes('appzenfreetrial'),
    duration:
      isDefined(log.start_time) && moment(log.start_time).isValid()
        ? moment(endTime).diff(moment(log.start_time)) / 1000
        : null,
    api_endpoint: log.api_endpoint,
    api_method: log.api_method,
    app_name: 'system_integration',
    ...otherLogDetails,
  };

  const stringifiedLog = JSON.stringify(newLog);
  if (log.level === LOG_LEVEL_TYPE.ERROR) {
    console.error(stringifiedLog);
  } else {
    console.log(stringifiedLog);
  }
  return;
};
