import moment from 'moment';
import type { Client as ElasticClient } from '@elastic/elasticsearch/api/new';
import { INTEGRATIONS_API_BASE_PATH } from '../../common/constants/common';
import { useElasticClient } from '../client/elasticsearch_client';
import { AuthUserDetails, ESDocument, ESSearchResult, RouteDependencies } from '../types';
import { ISO_DATE_FORMAT } from '../constants/date';
import { logEvent } from '../utility/log_event';
import { LOG_LEVEL_TYPE, RESPONSE_STATUS } from '../constants/log';
import {
  GetIntegrationObjectDetailsQuerySchema,
  getIntegrationObjectDetailsQuerySchema,
} from '../../common/api_schemas/integration_object';
import { IntegrationObject } from '../../common/types/integration_object';
import { mockIntegrationObjectResponse } from '../mocks/integration_object_response';

export function registerIntegrationObjectRoutes(routeDependencies: RouteDependencies) {
  const { router, config, auth, logger } = routeDependencies;
  const esClient: ElasticClient = useElasticClient(config);

  router.get<undefined, undefined, getIntegrationObjectDetailsQuerySchema>(
    {
      path: `${INTEGRATIONS_API_BASE_PATH}integration-objects`,
      validate: {
        query: GetIntegrationObjectDetailsQuerySchema,
      },
    },
    async (_ctx, req, res) => {
      const startTime = moment().utc().format(ISO_DATE_FORMAT);
      const { customerId } = auth.get(req)?.state as AuthUserDetails;

      const loggerInfo = {
        event: 'GET_INTEGRATION_OBJECTRS',
        api_endpoint: `${INTEGRATIONS_API_BASE_PATH}integration-objects`,
        api_method: 'GET',
        start_time: startTime,
        user_details: auth.get(req)?.state as AuthUserDetails,
      };

      const QueryParamsString = `Request Params: ${JSON.stringify(req.params)}`;

      try {
        if (!customerId) {
          logEvent(
            {
              ...loggerInfo,
              ...{
                level: LOG_LEVEL_TYPE.ERROR,
                status_code: RESPONSE_STATUS.FORBIDDEN,
                msg: `${QueryParamsString}\nError: Customer ID not present.`,
              },
            },
            null,
            logger
          );
          return res.forbidden();
        }

        const integrationObjectSearchResp: ESSearchResult<IntegrationObject> = mockIntegrationObjectResponse;
        const integrationObjects = integrationObjectSearchResp.body.hits.hits.map(
          (hit: ESDocument<IntegrationObject>) => ({ ...hit._source, id: hit._id })
        );

        logEvent(
          {
            ...loggerInfo,
            ...{
              level: LOG_LEVEL_TYPE.INFO,
              status_code: RESPONSE_STATUS.OK,
              msg: `${QueryParamsString}\nResponse: ${JSON.stringify({
                results: integrationObjects,
                count: integrationObjectSearchResp.body.hits.total.value,
              })}`,
            },
          },
          null,
          logger
        );
        return res.ok({
          body: { results: integrationObjects, count: integrationObjectSearchResp.body.hits.total.value },
        });
      } catch (e) {
        logEvent(
          {
            ...loggerInfo,
            ...{
              level: LOG_LEVEL_TYPE.ERROR,
              status_code: RESPONSE_STATUS.INTERNAL_SERVER_ERROR,
              msg: `${QueryParamsString}\nError: ${e}`,
            },
          },
          null,
          logger
        );
        return res.internalError();
      }
    }
  );
}
