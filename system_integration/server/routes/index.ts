import { RouteDependencies } from '../types';
import { registerIntegrationObjectRoutes } from './integration_object';

export class ApiRoutes {
  setup(dependencies: RouteDependencies) {
    registerIntegrationObjectRoutes(dependencies);
  }

  start() {}
  stop() {}
}
