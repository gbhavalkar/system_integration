import { HttpAuth, IRouter, Logger } from 'src/core/server';
import { ConfigSchema } from './config';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface SystemIntegrationPluginSetup {}
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface SystemIntegrationPluginStart {}

export interface RouteDependencies {
  router: IRouter;
  config: ConfigSchema;
  auth: HttpAuth;
  logger: Logger;
}

export interface ESSearchResult<T> {
  body: {
    hits: {
      total: {
        value: number,
      },
      hits: Array<ESDocument<T>>
    }
  }
};

export interface ESDocument<T> {
  _id: string,
  _source: T
};

export interface AuthUserDetails {
  token: string;
  csrfToken?: string;
  sessionId: string;
  domainName: string;
  customerId: string;
  azUserId: string;
  emailAddress: string;
  firstName: string;
  lastName: string;
  customerName: string;
  roles: string[];
  emulatedByAppzenSuperAdmin: boolean;
  emulatedByPartnerAdmin: boolean;
  emulatedByEmulator: boolean;
  entitySettings: EntitySetting[];
  createdBy?: string;
}

export interface RefreshTokenResponse extends Record<string, any> {
  fiat: string;
  iat: string;
  exp: string;
  token: string;
}

export interface TokenValidationError {
  error: boolean;
}

interface UserDetailsRole {
  authority: string;
}

export interface UserDetails {
  az_user_id: string;
  first_name: string;
  last_name: string;
  customer_id: string;
  customer_name: string;
  user_roles: UserDetailsRole[];
  emulated_by_appzen_super_admin: boolean;
  emulated_by_concur_partner_admin: boolean;
  emulated_by_emulator: boolean;
  created_by?: string;
}

export interface Entity {
  entity_name: string;
  entity_id: string;
}

export interface EntitySetting {
  app_name: string;
  entities: Entity[];
}

export interface UserEntities {
  customer_id: string;
  az_user_id: string;
  entity_settings: EntitySetting[];
}

