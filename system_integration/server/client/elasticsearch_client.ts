import { Client } from '@elastic/elasticsearch';
import type { Client as ElasticClient } from '@elastic/elasticsearch/api/new';
import { ConfigSchema } from '../config';

export const useElasticClient = (config: ConfigSchema) => {
  const elasticClient: ElasticClient = process.env.DATA_ELASTICSEARCH_USERNAME
  ? new Client({
    node: config.dataESEndpoint,
    auth: {
      username: process.env.DATA_ELASTICSEARCH_USERNAME || '',
      password: process.env.DATA_ELASTICSEARCH_PASSWORD || '',
    },
    ssl: {
      rejectUnauthorized: false,
    },
  })
  : new Client({
    node: config.dataESEndpoint,
  });

  return elasticClient;
}
