import { schema, TypeOf } from '@kbn/config-schema';

export const config = {
  schema: schema.object({
    dataESEndpoint: schema.string({ defaultValue: 'https://local-dev-mastermind-ui-es.ingress-zero.ds.dev.appzen.com' }),
    processServiceEndpoint: schema.string({ defaultValue: 'https://process-engine.ingress-zero.ds.dev.appzen.com' }),
    usersByRolesApiURL: schema.string({ defaultValue: 'https://localhost:8080/console/rest/users/searchByRoles' }),
  }),
};

export type ConfigSchema = TypeOf<typeof config.schema>;
