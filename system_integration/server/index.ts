import { PluginInitializerContext, PluginConfigDescriptor } from '../../../src/core/server';
import { SystemIntegrationPlugin } from './plugin';
import { TypeOf } from '@kbn/config-schema';
import { config as pluginConfig } from './config';

//  This exports static code and TypeScript types,
//  as well as, Kibana Platform `plugin()` initializer.

type ConfigType = TypeOf<typeof pluginConfig.schema>;

export function plugin(initializerContext: PluginInitializerContext) {
  return new SystemIntegrationPlugin(initializerContext);
}
export const config: PluginConfigDescriptor<ConfigType> = {
  schema: pluginConfig.schema,
  exposeToBrowser: {},
};
export { SystemIntegrationPluginSetup, SystemIntegrationPluginStart } from './types';
