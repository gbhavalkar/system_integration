export enum LOG_LEVEL_TYPE {
  ERROR = 'error',
  WARNING = 'warning',
  INFO = 'info'
}

export enum RESPONSE_STATUS {
  OK = 200,
  INTERNAL_SERVER_ERROR = 500,
  FORBIDDEN = 403,
  BAD_REQUEST = 400
}