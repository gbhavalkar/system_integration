export const isDefined = (value?: any | null) => {
  return value !== undefined && value !== null && value !== '';
}

export default isDefined;