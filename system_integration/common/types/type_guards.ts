import { ConsoleUserProfile } from "./user_profile";

export const isValidUserProfile =  (userProfile: ConsoleUserProfile): userProfile is ConsoleUserProfile => {
  return (userProfile as ConsoleUserProfile).azUserId !== undefined && (userProfile as ConsoleUserProfile).customerId !== undefined;
}