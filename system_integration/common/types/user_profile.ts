export interface ConsoleUserProfile {
  customerId: string;
  azUserId: string;
  customerName: string;
  emailAddress: string;
  emulatedByAppzenSuperAdmin: boolean;
  emulatedByPartnerAdmin: boolean;
  emulatedByEmulator: boolean;
  firstName: string;
  lastName: string;
  roles: string[];
}