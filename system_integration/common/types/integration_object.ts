import { IntegrationTypes, TransportationMethods } from "../constants/integration_object";

export interface IntegrationObject {
  integration_id: string,
  integration_type: IntegrationTypes,
  integration_name: string,
  customer_id: string,
  transportation_method?: TransportationMethods
}