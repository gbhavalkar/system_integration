import { schema, TypeOf } from '@kbn/config-schema';
import { IntegrationTypes, TransportationMethods } from '../constants/integration_object';

export const INTEGRATION_TYPES = schema.oneOf([
  schema.literal(IntegrationTypes.COUPA),
  schema.literal(IntegrationTypes.CSV),
]);

export const TRANSPORTATION_METHODS = schema.oneOf([
  schema.literal(TransportationMethods.BOX),
  schema.literal(TransportationMethods.SFTP),
]);

export const CredentialAuthenticationSchema = schema.object({
  url: schema.string(),
  user_id: schema.string(),
  password: schema.string(),
});

export const KeyAuthenticationSchema = schema.object({
  url: schema.string(),
  api_key: schema.string(),
});

export const GetIntegrationObjectDetailsQuerySchema = schema.object({
  pageSize: schema.number(),
  pageNumber: schema.number(),
});
export type getIntegrationObjectDetailsQuerySchema = TypeOf<typeof GetIntegrationObjectDetailsQuerySchema>;

export const GetIntegrationObjectsSchema = schema.object({
  count: schema.number(),
  results: schema.arrayOf(
    schema.object({
      customer_id: schema.string(),
      integration_id: schema.string(),
      integration_type: INTEGRATION_TYPES,
      integration_name: schema.string(),
      transportation_method: TRANSPORTATION_METHODS,
    })
  ),
});
export type GetIntegrationObjectsSchema = TypeOf<typeof GetIntegrationObjectsSchema>;

export const GetIntegrationObjectDetailsSchema = schema.object({
  customer_id: schema.string(),
  integration_id: schema.string(),
  integration_type: INTEGRATION_TYPES,
  integration_name: schema.string(),
  transportation_method: TRANSPORTATION_METHODS,
  transportation_configuration: schema.object({
    sftp: schema.maybe(CredentialAuthenticationSchema),
    box: schema.maybe(CredentialAuthenticationSchema),
    coupa: schema.maybe(KeyAuthenticationSchema),
  }),
});
export type GetIntegrationObjectDetailsSchema = TypeOf<typeof GetIntegrationObjectDetailsSchema>;

export const UpdateIntegrationObjectSchema = schema.object({
  integration_type: INTEGRATION_TYPES,
  integration_name: schema.string(),
  transportation_method: TRANSPORTATION_METHODS,
  transportation_configuration: schema.object({
    sftp: schema.maybe(CredentialAuthenticationSchema),
    box: schema.maybe(CredentialAuthenticationSchema),
    coupa: schema.maybe(KeyAuthenticationSchema),
  }),
});
export type UpdateIntegrationObjectSchema = TypeOf<typeof UpdateIntegrationObjectSchema>;
