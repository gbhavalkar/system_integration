export enum IntegrationTypes {
  COUPA = 'COUPA',
  CSV = 'CSV'
};

export enum TransportationMethods {
  BOX = 'BOX',
  SFTP = 'SFTP'
};

export enum IntegrationObjectWorkbenchFields {
  INTEGRATION_OBJECT_NAME = 'integration_name',
  INTEGRATION_OBJECT_TYPE = 'integration_type'
}