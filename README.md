# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Repo structure ###

* This repository contains the System Integration project code.
* `system_integration` contains the plugin which needs to be copied in the plugins folder of the mastermind kibana repo.
* `schema` contains all the schemas of elasticsearch index mappings along with the process trigger jsons if any.
